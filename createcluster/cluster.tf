variable "client_id" {
    description = "Will come from env var"
}

variable "client_secret" {
    description = "Will come from env var"
}

resource "azurerm_kubernetes_cluster" "k8s" {
  name                = "cs-test"
  location            = azurerm_resource_group.k8s.location
  resource_group_name = azurerm_resource_group.k8s.name
  dns_prefix          = "cs-test"
  kubernetes_version  = "1.28.12"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2s_v3" # "standard_E2as_v5" # "Standard_D2_v3"
    os_disk_size_gb = 30
  }

  network_profile {
    network_plugin     = "azure"
    load_balancer_sku  = "basic"
  }

  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }

  role_based_access_control_enabled = true

  tags = {
    Environment = "Production"
  }
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.k8s.kube_config_raw
  sensitive = true
}

output "host" {
  value = azurerm_kubernetes_cluster.k8s.kube_config.0.host
  sensitive = true
}