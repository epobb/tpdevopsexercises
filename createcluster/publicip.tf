resource "azurerm_public_ip" "default-ip" {
  name                = "defaultIp"
  location            = azurerm_resource_group.k8s.location
  resource_group_name = azurerm_resource_group.k8s.name
  allocation_method   = "Static"
  sku                 = "Basic"

  tags = {
    Environment = "Production"
  }
}

output "publicIp" {
  value = azurerm_public_ip.default-ip.ip_address
}

resource "azurerm_public_ip" "back-ip" {
  name                = "backIp"
  location            = azurerm_resource_group.k8s.location
  resource_group_name = azurerm_resource_group.k8s.name
  allocation_method   = "Static"
  sku                 = "Basic"

  tags = {
    Environment = "Production"
  }
}

output "backIp" {
  value = azurerm_public_ip.back-ip.ip_address
}
